@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
  <div class="wrapper bgded overlay" style="background-image:url('assets/images/home-slide.jpeg');">
    <div id="pageintro" class="hoc clear"> 
    <!--<img src="assets/images/home-slide.jpeg"  class="img-fluid"> -->
    <article>
      <h1 class="heading text-center">LAUNCHING SOON!</h1>
      <p class="text-center">APP LIKE NO OTHER!</p>
      <footer1>
        <ul class="nospace inline pushright">
          <li><a class="btn" href="#">New NoNamer</a></li>
          <li><a class="btn inverse" href="generate-otp">Existing NoNamer</a></li>
        </ul>
      </footer1>
    </article> 
    </div>           
  </div>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
              <div class="card-body">
               <img src="assets/images/home-side.png"  class="img-fluid">
              </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
              <div class="card-body">
<p><strong>Online dating through applications</strong> are location based mobile applications created to make communication easier for people who want to meet, flirt, chat, and potentially get romantically involved. This is a form of mobile dating or online dating specifically for smartphone users. Since the first app launch.</p>
<div class="row py-4">
<div class="col-md-3 col-sm-3 col-xs-3">
<img src="assets/images/appStore.png"  class="img-fluid app-icon py-4">
</div>
<div class="col-md-3 col-sm-3 col-xs-3">
<img src="assets/images/googleStore.png"  class="img-fluid app-icon">
</div>
</div>
              </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
  window.onload = function() {
    document.getElementById("myAudio").play(); 
}
  // Set the date we're counting down to
var countDownDate = new Date("Mar 9, 2019 00:00:00").getTime();
// Update the count down every 1 second

var x = setInterval(function() {
// Get todays date and time
var now = new Date().getTime();
var distance = countDownDate - now;
var days = Math.floor(distance / (1000 * 60 * 60 * 24));
days=days <10 ? '0'+days:days;
var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
hours=hours <10 ? '0'+hours:hours;
var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
minutes=minutes <10 ? '0'+minutes:minutes;
var seconds = Math.floor((distance % (1000 * 60)) / 1000);
seconds=seconds <10 ? '0'+seconds:seconds;
//document.getElementById("timer").innerHTML = days + "d " + hours + "h "

//+ minutes + "m " + seconds + "s ";

document.getElementById("timer").innerHTML = days + ":" + hours + ":"

  + minutes + ":" + seconds;
  
  if (distance < 0) {
  clearInterval(x);
  document.getElementById("timer").innerHTML = "START";
  }
}, 1000);

</script>
@endsection
@extends('layouts.head')







