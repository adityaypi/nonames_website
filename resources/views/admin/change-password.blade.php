@include('admin.include.head')
@include('admin.include.header') 
<!-- end::Head -->
<!-- end::Body -->

<!-- begin::Body -->
@include('admin.include.side-menu')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Home
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Change Password
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <form method="post" action="" id="changePasswordFrm">
        {{ csrf_field()}}
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-wpforms"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Change Password
                                </h3>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body">
                        <div class="row justify-content-md-center">
                            <div class="col-lg-7">

                                <h5>Use the form below to change your password.</h5><br>
                                @if ( session()->has('message') )
                                <div class="alert alert-success display-hide" style="display: block;">
                                    <button class="close" data-close="alert"></button>
                                    <span>{{ session()->get('message') }}</span>
                                </div>
                                @endif
                                @foreach($errors->all() as $error)
                                <div class="alert alert-danger display-hide" style="display: block;">
                                    <button class="close" data-close="alert"></button>
                                    <span>{{ $error }}</span>
                                </div>
                                @endforeach
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-lg-3 col-form-label">
                                        Enter Old Password
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control m-input" type="password" placeholder="Old Password" name="old_password" id="old_password">
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-lg-3 col-form-label">
                                        Enter New Password
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control m-input" type="password" placeholder="New Password" name="new_password" id="new_password">
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-lg-3 col-form-label">
                                        Confirm New Password
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control m-input" type="password" placeholder="Confirm New Password" name="confirm_password" id="confirm_password">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-12 text-right">
                                <button type="submit" class="btn btn-warning">
                                    Save
                                </button>
                                <button type="submit" class="btn btn-secondary">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <!--end::Portlet-->
            </div>
        </div>
        </form>
        <!--End::Section-->
    </div>
</div>
</div>
<!-- end:: Body -->
@include('admin.include.footer')
<script type="text/javascript">
    $(document).ready(function(){ 
        $("#changePasswordFrm").validate({
            rules: {
                old_password: {
                    required: true
                },
                new_password: {
                    required: true,
                    minlength:6
                },
                confirm_password: {
                    required: true,
                    minlength:6,
                    equalTo:"#new_password"
                }
            },
            messages: {
                old_password: {
                    required:"Please enter old password.",
                },
                new_password: {
                    required:"Please enter new password.",
                },
                confirm_password: {
                    required:"Please enter confirm password.",
                }
            }
        });
    });
</script>

