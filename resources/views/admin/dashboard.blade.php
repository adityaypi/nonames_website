<!doctype html>
<html lang="en">

    @include('admin.include.head') 
    @include('admin.include.header') 

    <!-- end::Head -->
    <!-- end::Body -->

    <!-- begin::Body -->
    @include('admin.include.side-menu')
    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Home
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>                        
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    Dashboard
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <style>
            .m-pricing-table-2 .m-pricing-table-2__head{padding: 50px 0}
            .m-pricing-table-2 .m-pricing-table-2__head .m-pricing-table-2__title{margin: 0}
            .m-pricing-table-2 .m-pricing-table-2__head hr{margin: 50px 0}
            .m-pricing-table-2 .m-pricing-table-2__head .btn-group a.btn{border-left: 1px solid #d2d2d2; margin-right: inherit; margin-left:-4px;}
            .m-pricing-table-2 .m-pricing-table-2__head .btn-group a.btn:first-child{border-left:0;}
        </style>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet">
                <div class="m-portlet__body m-portlet__body--no-padding">
                    <div class="m-pricing-table-2" style="background-color:#fff;">
                        <div class="m-pricing-table-2__head" style="height: 400px; font-size: 40px; background-color:#fff;">
                                Welcome To No Names Admin Section
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
            <!--End::Section-->
        </div>
    </div>
</div>
<!-- end:: Body -->
@include('admin.include.footer')

