@include('admin.include.head')
@include('admin.include.header') 
<!-- end::Head -->
<!-- end::Body -->

<!-- begin::Body -->
@include('admin.include.side-menu')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Home
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo Admin() ?>dashboard" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>                    
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Driver List
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="col-md-4 col-xl-4">
                            <div class="m-input-icon m-input-icon--left" style="margin-top:12px; text-align: right;">
                                <input type="text" class="form-control m-input" placeholder="Search by name..." id="generalSearch">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span>
                                        <i class="la la-search"></i>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">

                            
                          <input type="hidden" value="{{ csrf_token() }}" id="token">
                            <div class="m_datatable_user" id="tbl_driver_list"></div>
                            
                            
                            </div>
                        </div>
                        
                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--End::Section-->
    </div>
</div>
@include('admin.include.footer')
<script type="text/javascript">
  var datatable;
  var ADMIN = '<?php echo Admin(); ?>';
  //alert(ADMIN);
  var AjaxBasedDatatables = function() {
      //== Private functions
      var qstring = "";

      @if(!empty(app('request')->input('company_id')))
      qstring += 'company_id=' + {{ app('request')->input('company_id') }};
      @endif

      var companyInit = function() {
         datatable = $('#tbl_driver_list').mDatatable({
              // datasource definition
              data: {
                  type: 'remote',
                  source: {
                      read: {
                          method: 'GET',
                          url: ADMIN + 'ajaxListDriver',
                          map: function(raw) {
                              // console.log(raw);
                              var dataSet = raw;
                              if (typeof raw.data !== 'undefined') {
                                  dataSet = raw.data;
                              }
                              return dataSet;
                          },
                      },
                  },
                  pageSize: 10,
                  serverPaging: true,
                  serverFiltering: true,
                  serverSorting: true,
              },

              // layout definition
              layout: {
                  scroll: false,
                  footer: false
              },

              // column sorting
              sortable: true,

              pagination: true,

              toolbar: {
                  // toolbar items
                  items: {
                      // pagination
                      pagination: {
                          // page size select
                          pageSizeSelect: [10, 20, 30, 50, 100]
                      },
                  },
              },

              search: {
                  input: $('#generalSearch'),
              },

              rows: {
                  // auto hide columns, if rows overflow
                  autoHide: false
              },

              // columns definition
              columns: [{
                  field: 'id',
                  title: '#',
                  sortable: false, // disable sort for this column
                  width: 40,
                  selector: false,
                  textAlign: 'center',
                  template: function(row, index, datatable) {
                      return datatableCounter(datatable, index);
                  }
              }, {
                  field: 'name',
                  title: 'Name',
                  width: 120,
              },{
                  field: 'phone',
                  title: 'Phone',
                  width: 120,
              },{
                  field: 'created_on',
                  title: 'Added On',
                  width: 100,
                  template: function(row) {
                      return row.created_on.split(" ")[0]
                  }
              },{
                  field: 'is_deleted',
                  title: 'Status',
                  width: 100,
                  // callback function support for column rendering
                  template: function(row) {

                      var is_deleted = {
                          0: {
                              'title': 'Inactive',
                              'class': ' m-badge--danger'
                          },
                          1: {
                              'title': 'Active',
                              'class': ' m-badge--success'
                          },
                      };

                      var status_button = '<span id="sl' + row.id + '"><a href="javascript:void(0)" class="m-badge ' + is_deleted[row.is_deleted].class + ' m-badge--wide sts" title="Click to ' + (is_deleted[row.is_deleted].title == 'Active' ? 'Inactive' : 'Active') + '" data-status="' + row.is_deleted + '" data-id="' + row.id + '" data-token="{{ csrf_token() }}">' + is_deleted[row.is_deleted].title + '</a></span>';

                      status_button += '<div class="m-loader m-loader--brand m--hide" style="width: 30px; display: inline-block;" id="spin' + row.id + '"></div>';

                      return status_button;
                  }
              }, {
                  field: 'Actions',
                  width: 110,
                  title: 'Actions',
                  sortable: false,
                  overflow: 'visible',
                  template: function(row, index, datatable) {
                      var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';

                      return '\
                            <a href="' + ADMIN + 'driver/view/' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="View"><i class="la la-eye"></i>\
                             </a>\
                            <a href="javascript:void(0)" class="m-portlet__navlink btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete" title="Delete" data-id="' + row.id + '" data-token="{{ csrf_token() }}">\
                                 <i class="la la-trash"></i>\
                             </a>\
                            ';
                  },
              }],
          });

          $('#m_form_status').on('change', function() {
              datatable.search($(this).val().toLowerCase(), 'Status');
          });
          $('#company_id').on('change', function() {
              datatable.search($(this).val().toLowerCase(), 'Company');
          });

          $('#m_form_status,#company_id').selectpicker();

      };

      return {
          // public functions
          init: function() {
              companyInit();
          },
      };
  }();

  jQuery(document).ready(function() {

      AjaxBasedDatatables.init();

      $('.m_datatable_user').on('click', '.delete', function(e) {
          var id = $(this).attr('data-id');
          var token = $(this).attr('data-token');
          var ths = $(this);
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Yes, delete it!'
          }).then(function(result) {
              if (result.value) {
                  if (id != '') {
                      $.ajax({
                          type: 'post',
                          url: ADMIN +  "deleteDriver",
                          data: 'id=' + id + '&_token=' + token,
                          success: function(data) {
                              $('#' + id).hide();
                              $(ths).closest("table").closest("tr").hide();
                              datatable.reload();
                          }
                      })
                  }
                  swal(
                      'Deleted!',
                      'Site has been deleted.',
                      'success'
                  )
              }
          });
      });

      $('.m_datatable_user').on('click', '.sts', function() {
          var status_type = $(this).attr('data-status');
          var id = $(this).attr('data-id');
          var token = $(this).attr('data-token');

          if (status_type != '') {
              $.ajax({
                  type: 'post',
                  url: ADMIN +  "changeDriverStatus",
                  data: 'status=' + status_type + '&id=' + id + '&_token=' + token,
                  beforeSend: function() {
                      $('#spin' + id).removeClass('m--hide');
                  },
                  success: function(data) {
                      $('#spin' + id).addClass('m--hide');
                      if (status_type == 0) {
                          var e = '<a href="javascript:void(0)" class="m-badge m-badge--success m-badge--wide sts" title=" Click to Inactive" data-status="1" data-id="' + id + '" data-token="{{ csrf_token() }}">Active</a>';
                      } else {
                          var e = '<a href="javascript:void(0)" class="m-badge m-badge--danger m-badge--wide sts" title=" Click to Active"  data-status="0" data-id="' + id + '" data-token="{{ csrf_token() }}">Inactive</a>';
                      }
                      $("#sl" + id).html(e);
                  }
              })
          }
      });
  });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".checkAll").on('change',function(e){
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });
        
        $("#deleteAll").click(function(){
            if($("input[type='checkbox'][id='deleteId']:checked").length == 0){
                alert('Please select atleast one checkbox');
                return false;
            }else {
                if(confirm('Are you sure?')) {
                    return true;
                }
                else{
                    return false;
                }
            }
        });
    });
</script>
