@include('admin.include.head')
@include('admin.include.header') 
<!-- end::Head -->
<!-- end::Body -->
<!-- begin::Body -->
@include('admin.include.side-menu')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Home
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>                    
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Edit Profile
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <form method="post" action="" id="editProfileFrm">
        {{ csrf_field()}}
        <div class="row">
            <div class="col-xl-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-wpforms"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Edit Profile
                                </h3>
                            </div>
                        </div>
                    </div>
                    
                    <div class="m-portlet__body">
                               @if ( session()->has('message') )
                                <div class="alert alert-success display-hide" style="display: block;">
                                    <button class="close" data-close="alert"></button>
                                    <span>{{ session()->get('message') }}</span>
                                </div>
                            @endif
                        <div class="row justify-content-md-center">
                            <div class="col-lg-7">

                             <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-lg-3 col-form-label">
                                        Type
                                    </label>
                                    <div class="col-lg-9">
                                        <input type="text" name="type" class="form-control input-sm" id="type" readonly="" value="{{ (new \App\Helpers\Common)->userType($rowData->user_type) }}">
            
                                    </div>
                                </div>
                                

                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-lg-3 col-form-label">
                                        Email Address
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control m-input" type="text" name="email" id="email" placeholder="Email Address" value="<?php echo $rowData->email;?>">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-lg-3 col-form-label">
                                        Phone Number
                                    </label>
                                    <div class="col-lg-9">
                                        <input class="form-control m-input" type="text" name="phone" id="phone" placeholder="Phone Number" value="<?php echo $rowData->phone;?>">
                                    </div>
                                </div>
                                                              
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-12 text-right">
                                <button type="submit" class="btn btn-warning">
                                    Save
                                </button>
                                <button type="button" class="btn btn-secondary" onClick="window.location='{{ url('/admin/edit-profile') }}';">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <!--end::Portlet-->


            </div>
        </div>
        </form>
        <!--End::Section-->
    </div>
</div>
</div>
<!-- end:: Body -->
@include('admin.include.footer')
<script type="text/javascript">
    $(document).ready(function(){ 
        $("#state").change(function(event){
            var state=$('#state').val();
            var data='state='+state+'&_token='+"{{ csrf_token() }}";
            $.ajax({
                url: '{{url("/admin/ajax-find-state-code-by-state")}}',
                type: 'POST',
                data: data,
                success: function (response) {
                    $('#state_code').val(response);
                },
                error: function () {
                    $('#state_code').val('');
                }
            }); 
        });
        
        $("#editProfileFrm").validate({  
                rules: {
                    name: {
                        required: true
                    },
                    email:{  
                        required :true,
                        email :true,
                    }, 
                    phone:{
                        required: true,
                        number: true, 
                        minlength:10,
                        maxlength:10
                    },
                    address:{                           
                        required:true
                    },
                    state:{
                        required:true                           
                    },
                    state_code:{                           
                        required:true
                    }
                },
                messages: {
                    name:{
                        required: "Please enter name",
                    },
                    email: {
                        required: "Please enter email",
                        email: "Please enter valid email",
                    },
                    phone: {
                        required: "Please enter mobile no",
                        number:"Please enter valid mobile no",
                        minlength : "please enter 10 digits mobile number"
                    },
                    address:{
                        required: "Please enter address"
                    },
                    state:{
                        required: "Please select state"
                    },
                    state_code:{
                        required: "Please enter state code"
                    }
              }
        }); 
    });
</script>

