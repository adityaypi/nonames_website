<!DOCTYPE html>
<html lang="en" >
    <meta charset="utf-8" />
    <title>
        Nonames Admin
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{ asset('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="favicon icon" href="{{url('/assets/images/logo.png')}}" />
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
                <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
                    <div class="m-stack m-stack--hor m-stack--desktop">
                        <div class="m-stack__item m-stack__item--fluid">
                            <div class="m-login__wrapper">
                                <div class="m-login__logo">
                                    <a href="javascript:void(0)">
                                        
<!--                                        <img src="http://13.126.174.168/public/assets/app/media/img/logos/logo-2.png">-->
                                    </a>
                                </div>
                                <div class="m-login__signin">
                                    <div class="m-login__head">
                                        <h3 class="m-login__title">
                                            Sign In To Admin
                                        </h3>
                                    </div>


                                    <!-- BEGIN LOGIN FORM -->
                                    <form method="POST" action="" accept-charset="UTF-8" class="m-login__form m-form"><input name="_token" type="hidden" value="qQ7sL1Fv8MeIHdZ9F7aMH1USeM63q04g2kARqdum">
                                        {{ csrf_field()}}
                                        @if ( session()->has('message') )
                                        <div class="alert alert-danger" id="errorMessage">{{ session()->get('message') }}</div>
                                        @endif
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" value="">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" value="">
                                        </div>                                        
                                        <div class="m-login__form-action">
                                            <button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                                Sign In
                                            </button>
                                        </div>
                                    </form>
                                    <!-- END LOGIN FORM -->
                                </div>

                                <div class="m-login__forget-password">
                                    <div class="m-login__head">
                                        <h3 class="m-login__title">
                                            Forgotten Password ?
                                        </h3>
                                        <div class="m-login__desc">
                                            Enter your email to reset your password:
                                        </div>
                                    </div>

                                    <form method="POST" action="http://13.126.174.168/public/get_login" accept-charset="UTF-8" class="m-login__form m-form"><input name="_token" type="hidden" value="qQ7sL1Fv8MeIHdZ9F7aMH1USeM63q04g2kARqdum">
                                        <!-- BEGIN FORGOT PASSWORD FORM -->
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                                        </div>
                                        <div class="m-login__form-action">
                                            <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                                Request
                                            </button>
                                            <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                                                Cancel
                                            </button>
                                        </div>
                                    </form>
                                    <!-- END FORGOT PASSWORD FORM -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url(http://13.126.174.168/public/assets/app/media/img//bg/bg-4.jpg)">
                    <div class="m-grid__item m-grid__item--middle">
                        <h3 class="m-login__welcome">
                            Nonames
                        </h3>
                        <p class="m-login__msg">
                            Welcome to Nonames Admin
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Page -->
        <!-- begin::Base Scripts -->
        <script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
        <!--end::Page Snippets -->
    </body>
    <!-- end::Body -->
</html>

<script type="text/javascript">
        //== Class Definition
        var SnippetLogin = function () {

            var login = $('#m_login');


            var handleSignInFormSubmit = function () {
                $('#m_login_signin_submit').click(function (e) {
                    e.preventDefault();
                    var btn = $(this);
                    var form = $(this).closest('form');

                    form.validate({
                        rules: {
                            email: {
                                required: true,
                                email: true
                            },
                            password: {
                                required: true
                            }
                        }
                    });

                    if (!form.valid()) {
                        return;
                    }

                    btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                    form.ajaxSubmit({
                        url: "http://localhost/public/admin/get_login",
                        success: function (response, status, xhr, $form) {
                            if (response == 0) {
                                // similate 2s delay
                                setTimeout(function () {
                                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                                    showErrorMsg(form, 'danger', 'Incorrect username or password. <br/>Please try again.');
                                }, 2000);
                            } else {
                                window.location.href = "admin/dashboard";
                            }
                        }
                    });
                });
            }

        }();

</script>
