@include('admin.include.head')
@include('admin.include.header') 
<!-- end::Head -->
<!-- end::Body -->

<!-- begin::Body -->
@include('admin.include.side-menu')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Home
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo Admin() ?>dashboard" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>                    
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo Admin() ?>regester/list" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Register User
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <!-- END: Subheader -->
 <div class="m-content">
    <!--begin:: Widgets/Stats-->

    <div class="m-portlet">
        <div class="m-portlet__body">
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h5>Register Overview</h5>
                        <hr>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Name</label>
                            <div class="clearfix"></div>
                            <label for="exampleInputEmail1"><strong><strong>{{ $registerData->name }}</strong></strong></label>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Email</label>
                            <div class="clearfix"></div>
                            <label for="exampleInputEmail1"><strong>{{ $registerData->email }}</strong></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Phone</label>
                            <div class="clearfix"></div>
                            <label for="exampleInputEmail1"><strong>{{ $registerData->phone }}</strong></label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Biography</label>
                            <div class="clearfix"></div>
                            <label for="exampleInputEmail1"><strong>{{ $registerData->biography }}</strong></label>
                        </div>
                    </div>          
                </div>


        </div>
    </div>
    <!--End::Section-->
</div>
        <!--End::Section-->
    </div>
    </div>
</div>
<!-- end:: Body -->
@include('admin.include.footer')