@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
            <div class="card text-center">
                <div class="card-header">SPOKEN</div>
                <div class="card-body">

<form method="post" action="" id="addLanguage"  enctype="multipart/form-data">
<select name="language" id="languageList">
  <option value="0" selected>Select Language</option>
</select>
<div class="row" id="selectedLanguage">
<div class="container" id="2">US-ENGLISH <a id="l1" onclick="delLang(2)" class="btn btn-primary">X</a></div>
</div>
<input type="submit" value="Continue" id="ContinueBtn">
</form>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

	 var myLanguage = new Set();
   myLanguage.add(""+2);

$("#languageList").on('change',function(){
  if(!myLanguage.has(this.value)){
$("#selectedLanguage").append('<div class="container" id="'+this.value+'" >'+$("#languageList option:selected").html()+' <a id="l'+this.value+'" onclick="delLang('+this.value+')" class="btn btn-primary">X</a></div>');
}
myLanguage.add(this.value);
console.log(myLanguage);
});

function delLang(id)
{
  $('#'+id).remove();
myLanguage.delete(""+id);
}

$('form#addLanguage').submit(function (e) {
    //var str = $("form#verifyotp").serialize();
    var str="";
 for (let item of myLanguage)
 {
     str+=item+",";
 }
 str = str.replace(/,$/,"");
          e.preventDefault();
                        $.ajax({
                            url: 'api/add-user-language',
                            data: {language_id:str},
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                               $("#ContinueBtn").val("Please wait...");  
                            },
                            complete: function (data) {
                                  
                            },
                            success: function (data) {
                             
                                 if(data['code'])
                               {
                                  $("#ContinueBtn").val("Done");  
                                 window.location.href = 'update-body-type';
                               }else
                               {

                               }   
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                    
//all languages
$.ajax({
                            url: 'api/language',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                                
                            },
                            success: function (data) {

for ( i in data['payload']['allLanguage'])
{
  $("#languageList").append('<option value='+data['payload']['allLanguage'][i]['language_id']+'>'+data['payload']['allLanguage'][i]['language_name']+'</option>');
}



                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });



</script>


@endsection
@extends('layouts.head')
