@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                 <div class="card-header"><h2 class="pull-left"><a href="user-home">Back</a></h2><h2 class="text-center">Create Meetup</h2></div>
                <div class="card-body">
<form method="post" action="" enctype="" id="create_meetup">

<select id="event_id" name="event_id">
	
</select>
<input type="text" name="location" placeholder="Place & Location">
<input type="text" name="latitude" placeholder="latitude" value="28.234324324">
<input type="text" name="longitude" placeholder="longitude" value="70.354354534">
<input type="datetime-local" name="meetup_date_time">
Meet With: <input type="text" name="meet_with" value="2">
Meetup Contribute: <input type="text" name="meetup_contribute" value="1">
Club Table Female: <input type="text" name="club_table_female" value="2">
Club Table Male: <input type="text" name="club_table_male" value="1">
Club Male Split The Tab: <input type="text" name="club_male_split_the_tab" value="1">
Club Average Table Spand: <input type="text" name="club_average_table_spand" value="1">
Meetup Status: <input type="text" name="meetup_status" value="1">
<input type="submit" id="CreateMeetupBtn">
</form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

	
                        $.ajax({
                            url: 'api/events',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {
for(i in data['payload']['allEvent'])
{
event_id=data['payload']['allEvent'][i]['event_id'];
event_name=data['payload']['allEvent'][i]['event_name'];
icon_white=data['payload']['allEvent'][i]['icon_white'];
icon_dark=data['payload']['allEvent'][i]['icon_dark'];
status=data['payload']['allEvent'][i]['status'];
$("#event_id").append('<option value="'+event_id+'">'+event_name+'</option>');
//<img src="'+icon_dark+'"> 
}

                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });


$('form#create_meetup').submit(function (e) {

          e.preventDefault();
                        $.ajax({
                            url: 'api/create-meetup',
                         data:  new FormData(this),
                            type: 'POST',
                              "processData": false,
                                 dataType: 'json',
  "contentType": false,
  "mimeType": "multipart/form-data",
                            headers: {
    "Authorization": AUTH_ADITYA,
        "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                                $("#CreateMeetupBtn").val("Please wait..."); 
                               
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                               if(data['code'])
                               {
                                   $("#CreateMeetupBtn").val("Done"); 
                               	  //window.location.href = 'profile-update'; 
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });

</script>
@endsection
@extends('layouts.head')
