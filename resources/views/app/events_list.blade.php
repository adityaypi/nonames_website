@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">Pick as many as you like</div>
                <div class="card-body">
<form method="post" action="" id="adduserevent">
<div class="row" id="events">

</div>
<input type="submit" value="Continue" id="ContinueBtn">
</form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
//All Events
var image_white=[];
var image_dark=[];
//var str='<script>alert("hello");';
//str+="<";
//str+='<\/script>';


                        $.ajax({
                            url: 'api/events',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {
for(i in data['payload']['allEvent'])
{
event_id=data['payload']['allEvent'][i]['event_id'];
event_name=data['payload']['allEvent'][i]['event_name'];
icon_white=data['payload']['allEvent'][i]['icon_white'];
icon_dark=data['payload']['allEvent'][i]['icon_dark'];
status=data['payload']['allEvent'][i]['status'];
image_white[event_id]=icon_white;
image_dark[event_id]=icon_dark;
$("#events").append('<div class="col-md-4 events" id="event'+event_id+'" onclick="myfun(this,'+event_id+')" ><img src="'+icon_dark+'"/><p>'+event_name+'</p><input type="checkbox" hidden value="'+event_id+'" name="event_id[]"></div>');
}

                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });


$('form#adduserevent').submit(function (e) {
    //var str = $("form#verifyotp").serialize();
    var str="";
 for (let item of myEvents)
 {
     str+=item+",";
 }
 str = str.replace(/,$/,"");
 
          e.preventDefault();
                        $.ajax({
                            url: 'api/add-user-event',
                            data: {event_id:str},
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                               $("#ContinueBtn").val("Please wait...");  
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                             
                                 if(data['code'])
                               {
                                  $("#ContinueBtn").val("Done");  
                                   window.location.href = 'update-contribute';
                                 
                               }else
                               {

                               }   
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                    

                    
</script>



<script>
	var myEvents = new Set();
 
function myfun(str,id)
{

if($(str).find("input").is(":checked"))
{
    $(str).find('input:checkbox:first').removeAttr('checked');
    $(str).find("img").attr("src",image_dark[id]);
    myEvents.delete(id);
}else {
	$(str).find('input:checkbox:first').attr('checked', 'checked');
    $(str).find("img").attr("src",image_white[id]);
    myEvents.add(id);
}
//console.log(myEvents) ;
  
}


/*
$('.events').click(function () {

if ($(this).find('input:checkbox[name=markerType]').is(":checked")) {

  $(this).find('input:checkbox[name=markerType]').attr("checked", false);
}
else {
    $(this).find('input:checkbox[name=markerType]').prop("checked", true);
}

 alert($(this).find('input:checkbox[name=markerType]').is(":checked"));
});


$('input[type=checkbox]').click(function (e) {
   e.stopPropagation();
});

*/


 /* $.post("http://nonames.com/api/generate-otp",
  {
    phone: "9555727028"
   },
  function(data, status){
    alert("Data: " + data + "\nStatus: " + status);
  });
*/

</script>
@endsection
@extends('layouts.head')
