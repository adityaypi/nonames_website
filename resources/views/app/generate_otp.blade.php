@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<script type="text/javascript">
  if (sessionStorage.getItem("AUTHTOKEN") != null)
{
  window.location.href = '/user-home';
}
</script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">Generate OTP</div>
                <div class="card-body">
<form method="post" id="sendotp" enctype="">
<select name="phone_prefix" id="phone_prefix"><option value="91">+91</option><option value="1">+1</option></select>
<input type="text" name="phone" id="phone">
<div class="col-md-12">
<input type="submit" class="btn btn-primary" id="GenOtpBtn">
</div>
</form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myOTP" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div> -->
      <div class="modal-body">
      <div class="col-md-12">
      <span id="infoverify" class="text-info"></span>
      <form method="post" id="verifyotp" enctype="">
<input type="text" name="otp" id="otp" class="form-control" placeholder="Type OTP"> 
<input type="hidden" name="phone" id="phoneOTP"> 
<div class="col-md-12">
<input type="submit" class="btn btn-primary" value="Verify"  id="VerifyOtpBtn">
</div>
    </form>
    </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script>
 

   var phone_prefix=0;
$('form#sendotp').submit(function (e) {
    //var str = $("form#sendotp").serialize();
     phone_prefix=$("#phone_prefix").val();
    var phone=$("#phone").val(); 
    
          e.preventDefault();
                        $.ajax({
                            url: 'api/generate-otp',
                            data: {phone:phone},
                            type: 'POST',
                            dataType: 'json',
                            beforeSend: function () {
    $("#GenOtpBtn").val("Please wait...");                           
                            },
                            complete: function (data) {
                                $("#GenOtpBtn").val("Submit");     	
                            },
                            success: function (data) {
//data['payload']['phone']
                               if(data['code'])
                               {
                               // alert(data['payload']['otp']);
                               $("#phoneOTP").val(phone);
                               $("#otp").val(data['payload']['otp']);
                               $('#myOTP').modal('show'); 


                               }else{
                                   alert("Not Registered");
                               }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                    
</script>


<script>
$('form#verifyotp').submit(function (e) {
    var str = $("form#verifyotp").serialize();
   
          e.preventDefault();
                        $.ajax({
                            url: 'api/verify-otp',
                            data: str,
                            type: 'POST',
                            dataType: 'json',
                             beforeSend: function () {
            $("#VerifyOtpBtn").val("Please wait...");                   
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                                if(data['code'])
                               {
                                  $("#infoverify").html(data['message']);
                                  var phone=data['payload']['phone']; 
                                  $("#VerifyOtpBtn").val("Done");
//register
                        $.ajax({
                            url: 'api/register',
                            data: {phone:phone,device_type:'a',device_token:'fsdfdsfds',phone_prefix:phone_prefix},
                            type: 'POST',
                            dataType: 'json',

                            beforeSend: function () {
                          
                            },
                            complete: function (data) {
                                  
                            },
                            success: function (data) {
                               if(data['code'])
                               {
    sessionStorage.setItem("AUTHTOKEN", data['payload']['token']); 
 sessionStorage.setItem("USERID", data['payload']['id']); 

document.cookie = "AUTHTOKEN="+sessionStorage.getItem("AUTHTOKEN");
document.cookie = "USERID="+sessionStorage.getItem("USERID");

//document.cookie = "AUTHTOKEN="+sessionStorage.getItem("AUTHTOKEN")+";expires="+expire.toGMTString();
//document.cookie = "USERID="+sessionStorage.getItem("USERID")+";expires="+expire.toGMTString();  
window.location.href = 'update-gender';
                               }else{

                                  //login
                        $.ajax({
                            url: 'api/login',
                            data: {phone:phone,device_type:'a',device_token:'fsdfdsfds',phone_prefix:phone_prefix},
                            type: 'POST',
                            dataType: 'json',
                            
                            beforeSend: function () {
                          
                            },
                            complete: function (data) {
                                  
                            },
                            success: function (data) {
                               if(data['code'])
                               {

   sessionStorage.setItem("AUTHTOKEN", data['payload']['token']); 
 sessionStorage.setItem("USERID", data['payload']['id']); 
document.cookie = "AUTHTOKEN="+sessionStorage.getItem("AUTHTOKEN");
document.cookie = "USERID="+sessionStorage.getItem("USERID");
 
  // document.cookie = "AUTHTOKEN="+sessionStorage.getItem("AUTHTOKEN")+";expires="+expire.toGMTString();
//document.cookie = "USERID="+sessionStorage.getItem("USERID")+";expires="+expire.toGMTString();  
window.location.href = '/user-home';
                               }else{
                                   alert("Not Login");
                               }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                               }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

                                 //window.location.href = 'events-list';
                               }else{
                                $("#infoverify").html(data['message']); 
                                $("#VerifyOtpBtn").val("Submit");     
                               }
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                    
</script>

<script>
	

 /* $.post("http://nonames.com/api/generate-otp",
  {
    phone: "9555727028"
   },
  function(data, status){
    alert("Data: " + data + "\nStatus: " + status);
  });
*

</script>
@endsection
@extends('layouts.head')
