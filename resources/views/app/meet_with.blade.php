@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">Meet With</div>
                <div class="card-body">
<form method="post" action="" id="updatemeetwith">
<div class="row" id="meet_with">
<input type="radio" name="meet_with" value="1"> Male
<input type="radio" name="meet_with" value="2"> Female
<input type="radio" name="meet_with" value="3"> Both
</div>
<input type="submit" value="Continue" id="ContinueBtn">
</form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	

$('form#updatemeetwith').submit(function (e) {
    var str = $("form#updatemeetwith").serialize();

          e.preventDefault();
                        $.ajax({
                            url: 'api/update-meet-with',
                            data: str,
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                                $("#ContinueBtn").val("Please wait..."); 
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                             if(data['code'])
                               {
                                   $("#ContinueBtn").val("Done"); 
                               	   window.location.href = 'upload-image';
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                    

</script>
@endsection
@extends('layouts.head')
