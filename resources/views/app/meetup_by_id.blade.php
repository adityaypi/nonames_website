@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="card-header"><h2 class="pull-left"><a href="user-home">Back</a></h2><h2 class="text-center"></h2></div>
                <div class="card-body">
<div id="meetupDetail">
</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var meetup_id=<?php echo $_POST['meetup_id']; ?>;
// ALL MEETUP	
                        $.ajax({
                            url: 'api/meetup-by-id',
                            type: 'POST',
                            data:{meetup_id:meetup_id},
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {
                        	
user_id=data['payload']['meetupDetail']['user_id'];
event=data['payload']['meetupDetail']['event'];
meetup_date_time=data['payload']['meetupDetail']['meetup_date_time'];
meetup_status=data['payload']['meetupDetail']['meetup_status'];

//$("#meetupDetail").append('<img onerror="$(this).remove()"class="img-thumbnail" src="'+image[img]+'"/>');

location_event=data['payload']['meetupDetail']['location'];
meetupRequest=data['payload']['meetupDetail']['meetupRequest'];


$("#meetupDetail").append('<div id="mymeetup'+user_id+'" ><p>'+event+"<br>"+location_event+"<br>"+meetup_date_time+'</p>');
$("#meetupDetail").append('<table  class="">');
for(meetR in meetupRequest)
{
sender_id=meetupRequest[meetR]['sender_id'];
age=meetupRequest[meetR]['age'];
favorite=meetupRequest[meetR]['favorite'];
image=meetupRequest[meetR]['image'];
messageCount=meetupRequest[meetR]['messageCount'];
videoCount=meetupRequest[meetR]['videoCount'];
request_status=meetupRequest[meetR]['request_status'];
$("#meetupDetail").append('<tr><td><img onerror="$(this).remove()"class="img-thumbnail" src="'+image+'"/></td></tr>');
$("#meetupDetail").append('<tr><td>'+age+'</td></tr>');
if(request_status == 1)
{
   $("#meetupDetail").append('<tr><td><button type="button" onclick="MeetupRequestAccept('+meetup_id+','+sender_id+')">Accept Request</button></td></tr>'); 
}


}
$("#meetupDetail").append('</table></div>');
if( meetup_status != '4'){
$("#meetupDetail").append('<button onclick="CancelMeetup('+meetup_id+',4)">Cancel Meetup</button>');
}

/*for(i in data['payload']['meetupDetail'])
{

meetup_id=data['payload']['meetupDetail'][i]['meetup_id'];
event=data['payload']['meetupDetail'][i]['event'];
meetup_date_time=data['payload']['meetupDetail'][i]['meetup_date_time'];

image=data['payload']['meetupDetail'][i]['image'];

location_event=data['payload']['meetupDetail'][i]['location'];
occupation=data['payload']['meetupDetail'][i]['occupation'];
languages=data['payload']['meetupDetail'][i]['languages'];
body_type=data['payload']['meetupDetail'][i]['body_type'];
height=data['payload']['meetupDetail'][i]['height'];
smoke=data['payload']['meetupDetail'][i]['smoke'];
drink=data['payload']['meetupDetail'][i]['drink'];

$("#newnonamers").append('<div class="col-md-4 events" id="newnonamers'+user_id+'" ><img onerror="$(this).remove()"class="profile_img" src="'+image+'"/><p>'+age+'</p></div>'); 
}*/


                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

function MeetupRequestAccept(meetup_id, sender_id)
{

$.ajax({
                            url: 'api/meetup-request-accept',
                            type: 'POST',
                            data:{meetup_id:meetup_id,sender_id:sender_id},
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                                
                            },
                            success: function (data) {
                            


                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });


}

function CancelMeetup(meetup_id, meetup_status)
{

$.ajax({
                            url: 'api/cancel-meetup',
                            type: 'POST',
                            data:{meetup_id:meetup_id,meetup_status:meetup_status},
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                                
                            },
                            success: function (data) {
                            


                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });


}
                        </script>
@endsection
@extends('layouts.head')
