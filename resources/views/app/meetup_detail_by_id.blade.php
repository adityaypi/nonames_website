@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="card-header"><h2 class="pull-left"><a href="user-home">Back</a></h2><h2 class="text-center">Invites</h2></div>
                <div class="card-body">
<div id="meetupDetail">
</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

// ALL MEETUP	
                        $.ajax({
                            url: 'api/meetup-detail-by-id',
                            type: 'POST',
                            data:{meetup_id:<?php echo $_POST['meetup_id']; ?>},
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {
                            	
meetup_id=data['payload']['meetupDetail']['meetup_id'];
event=data['payload']['meetupDetail']['event'];
age=data['payload']['meetupDetail']['age'];
meetup_date_time=data['payload']['meetupDetail']['meetup_date_time'];
meetup_creator_id=data['payload']['meetupDetail']['meetup_creator_id'];

image=data['payload']['meetupDetail']['image'];
for (img in image)
{
$("#meetupDetail").append('<img onerror="$(this).remove()"class="img-thumbnail" src="'+image[img]+'"/>');
break;
}

location_event=data['payload']['meetupDetail']['location'];
occupation=data['payload']['meetupDetail']['occupation'];
languages=data['payload']['meetupDetail']['languages'];
body_type=data['payload']['meetupDetail']['body_type'];
height=data['payload']['meetupDetail']['height'];
smoke=data['payload']['meetupDetail']['smoke'];
drink=data['payload']['meetupDetail']['drink'];

$("#meetupDetail").append('<div id="newnonamers'+meetup_id+'" ><p>'+age+" "+location_event+'</p>\
    <table  class="table">\
    <tr><td>Occupation</td><td>'+occupation+'</td></tr>\
	<tr><td>Languages</td><td>'+languages+'</td></tr>\
	<tr><td>Body Type</td><td>'+Body_Type[body_type]+'</td></tr>\
	<tr><td>Height</td><td>'+height+'</td></tr>\
	<tr><td>Smoke</td><td>'+Smoke[smoke]+'</td></tr>\
	<tr><td>Drink</td><td>'+Drink[drink]+'</td></tr>\
	</table></div>');
$("#meetupDetail").append('<span id="Request"><h2 class="text-center" onclick="SendRequest('+meetup_id+','+meetup_creator_id+')">N</h2></span>');
/*for(i in data['payload']['meetupDetail'])
{

meetup_id=data['payload']['meetupDetail'][i]['meetup_id'];
event=data['payload']['meetupDetail'][i]['event'];
meetup_date_time=data['payload']['meetupDetail'][i]['meetup_date_time'];

image=data['payload']['meetupDetail'][i]['image'];

location_event=data['payload']['meetupDetail'][i]['location'];
occupation=data['payload']['meetupDetail'][i]['occupation'];
languages=data['payload']['meetupDetail'][i]['languages'];
body_type=data['payload']['meetupDetail'][i]['body_type'];
height=data['payload']['meetupDetail'][i]['height'];
smoke=data['payload']['meetupDetail'][i]['smoke'];
drink=data['payload']['meetupDetail'][i]['drink'];

$("#newnonamers").append('<div class="col-md-4 events" id="newnonamers'+user_id+'" ><img onerror="$(this).remove()"class="profile_img" src="'+image+'"/><p>'+age+'</p></div>'); 
}*/


                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

function SendRequest(meetup_id,meetup_creator_id)
{


 $.ajax({
                            url: 'api/meetup-request-sent',
                            type: 'POST',
                            data:{meetup_id:meetup_id,creator_id:meetup_creator_id},
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                          
                            },
                            success: function (data) {
                             $("#Request").html('<span id="Request"><h2 class="text-center" onclick="CancelRequest('+meetup_id+','+meetup_creator_id+')">N</h2></span>');   
                      
                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });




}

function CancelRequest(meetup_id,meetup_creator_id)
{


 $.ajax({
                            url: 'api/meetup-request-cancelled',
                            type: 'POST',
                            data:{meetup_id:meetup_id,creator_id:meetup_creator_id},
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                          SendRequest
                            },
                            success: function (data) {
                             $("#Request").html('<span id="Request"><h2 class="text-center" onclick="CancelRequest('+meetup_id+','+meetup_creator_id+')">N</h2></span>');   
                      
                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });




}



                        </script>
@endsection
@extends('layouts.head')
