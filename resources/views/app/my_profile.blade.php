@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<!-- col-md-6 col-md-offset-3 -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">User Home</div>
                <div class="card-body">
<div class="container">
<form method="post" action="" id="uploadimages" enctype="multipart/form-data">
    <div class="row">
<div class="col-md-4 invisible" id="divimg0">
<div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[0]" type="file" value="Choose a file">
    </div>
</div>
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" id="img0"  class="portimg" src="#"></div>
</div>
</div>

<div class="col-md-4 invisible" id="divimg1">
 <div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[1]" type="file" value="Choose a file">
     </div>
</div> 
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" id="img1"  class="portimg" src="#"></div>
</div>
</div>

<div class="col-md-4 invisible" id="divimg2">
 <div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[2]" type="file" value="Choose a file">
     </div>
</div> 
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" id="img2"  class="portimg" src="#"></div>
</div>
</div>

<div class="col-md-4 invisible" id="divimg3">
 <div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[3]" type="file" value="Choose a file">
     </div>
</div> 
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" id="img3"  class="portimg" src="#"></div>
</div>
</div>

<div class="col-md-4 invisible" id="divimg4">
 <div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[4]" type="file" value="Choose a file">
     </div>
</div> 
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" id="img4"  class="portimg" src="#"></div>
</div>
</div>

<div class="col-md-4 invisible" id="divimg5">
 <div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[5]" type="file" value="Choose a file">
     </div>
</div> 
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" id="img5"  class="portimg" src="#"></div>
</div>
</div>
<div class="col-md-12 text-center">
<input type="submit" value="Continue" id="ImageSubmitBtn" class="" hidden>
</div>
</div>
</form>
</div>
<ul class="nav nav-tabs">
  <li><a data-toggle="tab" href="#personal" class="active show">Personal</a></li>
  <li><a data-toggle="tab" href="#preferences">Preferences</a></li>
</ul>

<div class="tab-content">
  <div id="personal" class="tab-pane fade in active show">
    <form method="post" action="" id="updatepersonal">
<div class="row" id="personal">
  <input type="text" name="" id="phone">
  <input type="text" name="" id="id">
	<input type="hidden" name="gender" id="gender" class="form-control">
<input type="date" name="dob" id="dob" class="form-control">
<select name="profession" id="profession" class="form-control">
	
</select>
<input type="text" name="location" id="location" class="form-control">
<div class="row" id="selectedLanguage">

</div>
<input type="hidden" name="age" id="age" value="">
<input type="hidden" name="spoken" id="postspoken">
<select name="" id="spoken" class="form-control"><option value="0" selected>Select Language</option></select>
<select name="body_type" id="body_type">
	<option value="1">Slim</option>
	<option value="2">Athletic</option>
	<option value="3">Average</option>
	<option value="4">A few extra pounds</option>
</select>
<span id="height"></span>
<input type="text" name="height_inch" id="height_inch">
<input type="text" name="height_cm" id="height_cm">
<select name="drink" id="drink">
	<option value="1">No</option>
	<option value="2">Socially</option>
	<option value="3">Regularly</option>
</select>
<select name="smoke" id="smoke">
	<option value="1">No</option>
	<option value="2">Socially</option>
	<option value="3">Regularly</option>
</select>
</div>
<input type="submit" value="Save" class="invisible" id="SavePersonalBtn">
</form>
  </div>
  <div id="preferences" class="tab-pane fade">
      <form method="post" action="" id="updatepreferences">
    <h2>Meetup</h2>
    <div id="meetup" class="row">
    </div>
    <input type="hidden" name="event" id="event">
    <h2>With</h2>
    <div id="with">
<input type="radio" name="meet_with" id="meet_with1" value="1"> Male
<input type="radio" name="meet_with" id="meet_with2" value="2"> Female
<input type="radio" name="meet_with" id="meet_with3" value="3"> Both
    </div>
    <h2>$</h2>
    <div id="contribute">
      <input type="radio" name="contribute" id="contribute1" value="1"> Me
<input type="radio" name="contribute" id="contribute2" value="2"> You
<input type="radio" name="contribute" id="contribute3" value="3"> Split
    </div>
    <input type="submit" value="Save" class="invisible" id="SavePreferencesBtn">
  </form>
  </div>
</div>


                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

  //Second Tab
 var myLanguage = new Set();
var myEvents = new Set();

	$("#dob").change(function(){
var dob=$("#dob").val();

 var today = new Date();
    var birthDate = new Date($('#dob').val());
    var age = today.getFullYear() - birthDate.getFullYear();

    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
$("#age").val(age);
    });

$('form#updatepersonal').submit(function (e) {

          e.preventDefault();
                        $.ajax({
                            url: 'api/update-personal',
                         data:  new FormData(this),
                            type: 'POST',
                              "processData": false,
                                 dataType: 'json',
  "contentType": false,
  "mimeType": "multipart/form-data",
                            headers: {
    "Authorization": AUTH_ADITYA,
        "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                                $("#SavePersonalBtn").val("Please wait..."); 
                               
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                               if(data['code'])
                               {
                                   $("#SavePersonalBtn").val("Done"); 
                               	  window.location.href = 'my-profile'; 
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });


//all profession
 $.ajax({
                            url: 'api/profession',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {

for ( i in data['payload']['allProfession'])
{
	$("#profession").append('<option value='+data['payload']['allProfession'][i]['profession_id']+'>'+data['payload']['allProfession'][i]['name']+'</option>');
}



                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
//all languages
$.ajax({
                            url: 'api/language',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {

for ( i in data['payload']['allLanguage'])
{
	$("#spoken").append('<option value='+data['payload']['allLanguage'][i]['language_id']+'>'+data['payload']['allLanguage'][i]['language_name']+'</option>');
}


                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

	
$("#spoken").on('change',function(){
	if(!myLanguage.has(this.value)){
$("#selectedLanguage").append('<div class="container" id="'+this.value+'" >'+$("#spoken option:selected").html()+' <a id="l'+this.value+'" onclick="delLang('+this.value+')" class="btn btn-primary">X</a></div>');
}
myLanguage.add(this.value);
   var str="";
 for (let item of myLanguage)
 {
     str+=item+",";
 }
 str = str.replace(/,$/,"");
 $("#postspoken").val(str);
});


function delLang(id)
{
  $('#'+id).remove();
myLanguage.delete(""+id);
var str="";
 for (let item of myLanguage)
 {
     str+=item+",";
 }
 str = str.replace(/,$/,"");
 $("#postspoken").val(str);
}

$(window).load(function(){
// my profile	
                        $.ajax({
                            url: 'api/my-profile',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {

dob=data['payload']['dob'];
profession=data['payload']['profession'];
user_location=data['payload']['location'];
languages=data['payload']['languages'];
for(l in languages)
{ 
	
$("#selectedLanguage").append('<div class="container" id="'+languages[l]['id']+'" >'+languages[l]['name']+' <a id="l'+languages[l]['id']+'" onclick="delLang('+languages[l]['id']+')" class="btn btn-primary">X</a></div>');

myLanguage.add(""+languages[l]['id']);
   var str="";
 for (let item of myLanguage)
 {
     str+=item+",";
 }
 str = str.replace(/,$/,"");
  $("#postspoken").val(str);
}

body_type=data['payload']['body_type'];
height_cm=data['payload']['height_cm'];
height_inch=data['payload']['height_inch'];
drink=data['payload']['drink'];
smoke=data['payload']['smoke'];
gender=data['payload']['gender'];
age=data['payload']['age'];
phone=data['payload']['phone'];
meet_with=data['payload']['meet_with'];
contribute=data['payload']['contribute'];

id=data['payload']['id'];
events=data['payload']['events'];
eventarray=events.split(",");
for (ev in eventarray)
{
$("#eventimg"+eventarray[ev]).attr("src",image_white[eventarray[ev]]);
$("#check"+eventarray[ev]).attr('checked', 'checked');
    myEvents.add(eventarray[ev]);
      var str="";
 for (let item of myEvents)
 {
     str+=item+",";
 }
 str = str.replace(/,$/,"");
  $("#event").val(str);
}
$("#gender").val(gender);
$("#dob").val(dob);
$("#profession").val(profession);
$("#location").val(user_location);
$("#body_type").val(body_type);
$("#height_cm").val(height_cm);
$("#height_inch").val(height_inch);
$("#drink").val(drink);
$("#smoke").val(smoke);
$("#age").val(age);
$("#phone").val(phone);
$("#id").val(id);
$("#meet_with"+meet_with).attr('checked', 'checked');
$("#contribute"+contribute).attr('checked', 'checked');
var img=-1;
images=data['payload']['image'];
for(img in images)
{
  image_name=images[img]['name'];

  image_id=images[img]['id'];
  $("#img"+img).attr('src',images[img]['url']);
  $("#divimg"+img).removeClass('invisible');
  $("#img"+img).after('<button type="button" class="delbtn removebtn" value="'+image_id+","+image_name+'">Remove</button>');
//$("#images").append('<img src='+images[img]['url']+' class="profile_img">');
//$("#images").append('<div class="col-md-4"> <div class="form-group hirehide is-empty is-fileinput width100">    <div class="socialmediaside2">        <input  class="fileUpload" accept="image/jpeg, image/jpg" name="image[]" type="file" value="Choose a file">     </div></div><div class="upload-demo"><div class="upload-demo-wrap"><img alt="your image" class="portimg profile_img" src='+images[img]['url']+' ></div></div></div>');
}

if(img <= 5)
{
  img++;
  $("#divimg"+img).removeClass('invisible');
 }
$("#SavePersonalBtn").removeClass('invisible');
$("#SavePreferencesBtn").removeClass('invisible');

                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });


});


//Upload Images
  
      function readURL() {
        var $input = $(this);
               var $newinput =  $(this).parent().parent().parent().find('.portimg ');
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
             $('#ImageSubmitBtn').click();
                reset($newinput.next('.delbtn'), true);
                $newinput.attr('src', e.target.result).show();
               // $newinput.after('<input type="button" class="delbtn removebtn" value="remove">');
            }
            reader.readAsDataURL(this.files[0]);
        }
    }
    $(".fileUpload").change(readURL);
    $("form").on('click', '.delbtn', function (e) {
     image_id= $(this).val().split(",")[0];
     image_name=$(this).val().split(",")[1];
     reset($(this));
     DeleteImage(image_id,image_name);
    });

    function reset(elm, prserveFileName) {
        if (elm && elm.length > 0) {
            var $input = elm;
            $input.prev('.portimg').attr('src', '').hide();
            if (!prserveFileName) {
                $($input).parent().parent().parent().find('input.fileUpload ').val("");
                //input.fileUpload and input#uploadre both need to empty values for particular div
            }
            elm.remove();
        }
    }

function DeleteImage(image_id,image_name)
{


$.ajax({
                            url: 'api/delete-image',
                            type: 'POST',
                            data:{image_id:image_id,image_name:image_name},
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                                
                            },
                            success: function (data) {
                            
 window.location.href = 'my-profile'; 

                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

}

$('form#uploadimages').submit(function (e) {

          e.preventDefault();
                        $.ajax({
                            url: 'api/upload-images',
                         data:  new FormData(this),
                            type: 'POST',
                              "processData": false,
                                 dataType: 'json',
  "contentType": false,
  "mimeType": "multipart/form-data",
                            headers: {
    "Authorization": AUTH_ADITYA,
        "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                                $("#ImageSubmitBtn").val("Please wait..."); 
                            },
                            complete: function (data) {
                                  
                            },
                            success: function (data) {
                               if(data['code'])
                               {
                                   $("#ImageSubmitBtn").val("Done"); 
                                  window.location.href = 'my-profile'; 
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });

//Third Tab

//All Events
var image_white=[];
var image_dark=[];
//var str='<script>alert("hello");';
//str+="<";
//str+='<\/script>';

                        $.ajax({
                            url: 'api/events',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                                
                            },
                            success: function (data) {
for(i in data['payload']['allEvent'])
{
event_id=data['payload']['allEvent'][i]['event_id'];
event_name=data['payload']['allEvent'][i]['event_name'];
icon_white=data['payload']['allEvent'][i]['icon_white'];
icon_dark=data['payload']['allEvent'][i]['icon_dark'];
status=data['payload']['allEvent'][i]['status'];
image_white[event_id]=icon_white;
image_dark[event_id]=icon_dark;
$("#meetup").append('<div class="col-md-4 events" id="event'+event_id+'" onclick="myfun(this,'+event_id+')" ><img src="'+icon_dark+'" id="eventimg'+event_id+'"/><p>'+event_name+'</p><input id="check'+event_id+'" type="checkbox" hidden value="'+event_id+'" name=""></div>');
}

                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

$('form#updatepreferences').submit(function (e) {

          e.preventDefault();
                        $.ajax({
                            url: 'api/update-preferences',
                         data:  new FormData(this),
                            type: 'POST',
                              "processData": false,
                                 dataType: 'json',
  "contentType": false,
  "mimeType": "multipart/form-data",
                            headers: {
    "Authorization": AUTH_ADITYA,
        "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                                $("#SavePreferencesBtn").val("Please wait..."); 
                               
                            },
                            complete: function (data) {
                                  
                            },
                            success: function (data) {
                               if(data['code'])
                               {
                                   $("#SavePreferencesBtn").val("Done"); 
                                  window.location.href = 'my-profile'; 
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });

 
function myfun(str,id)
{

if($(str).find("input").is(":checked"))
{
    $(str).find('input:checkbox:first').removeAttr('checked');
    $(str).find("img").attr("src",image_dark[id]);
    myEvents.delete(""+id);
}else {
  $(str).find('input:checkbox:first').attr('checked', 'checked');
    $(str).find("img").attr("src",image_white[id]);
    myEvents.add(""+id);
}
  var str="";
 for (let item of myEvents)
 {
     str+=item+",";
 }
 str = str.replace(/,$/,"");
  $("#event").val(str);
//console.log(myEvents) ;
  
}

                        </script>
@endsection
@extends('layouts.head')
