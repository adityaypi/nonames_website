@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="card-header"></div>
                <div class="card-body">
<div id="nonamerDetail">
</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
// ALL MEETUP	
var route='<?php echo $_POST['route']; ?>';

                        $.ajax({
                            url: 'api/nonamer-detail-by-id',
                            type: 'POST',
                            data:{user_id:<?php echo $_POST['user_id']; ?>},
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {
                            	
user_id=data['payload']['user_id'];

image=data['payload']['image'];
for (img in image)
{
$("#nonamerDetail").append('<img onerror="$(this).remove()"class="img-thumbnail" src="'+image[img]+'"/>');
break;
}

age=data['payload']['age'];
occupation=data['payload']['occupation'];
languages=data['payload']['languages'];
body_type=data['payload']['body_type'];
height=data['payload']['height'];
smoke=data['payload']['smoke'];
drink=data['payload']['drink'];
$("#nonamerDetail").append('<div id="newnonamers'+user_id+'" ><p>'+age+'</p>\
    <table  class="table">\
    <tr><td>Occupation</td><td>'+occupation+'</td></tr>\
	<tr><td>Languages</td><td>'+languages+'</td></tr>\
	<tr><td>Body Type</td><td>'+Body_Type[body_type]+'</td></tr>\
	<tr><td>Height</td><td>'+height+'</td></tr>\
	<tr><td>Smoke</td><td>'+Smoke[smoke]+'</td></tr>\
	<tr><td>Drink</td><td>'+Drink[drink]+'</td></tr>\
	</table></div>');
if(route =="favorite")
{
$("#nonamerDetail").append('<span id="Request"><h2 class="text-center" onclick="DeleteUserFavorite('+user_id+','+USERID+')">F (Unfavorite)</h2></span>');
$(".card-header").html('<h2 class="pull-left"><a href="user-favorite">Back</a></h2><h2 class="text-center">Favorite</h2>');
}else if(route =="home")
{
   $("#nonamerDetail").append('<span id="Request"><h2 class="text-center" onclick="AddFavorite('+user_id+','+USERID+')">F</h2></span>'); 
   $(".card-header").html('<h2 class="pull-left"><a href="user-home">Back</a></h2><h2 class="text-center">New No Namer</h2>');
}

/*for(i in data['payload']['meetupDetail'])
{

meetup_id=data['payload']['meetupDetail'][i]['meetup_id'];
event=data['payload']['meetupDetail'][i]['event'];
meetup_date_time=data['payload']['meetupDetail'][i]['meetup_date_time'];

image=data['payload']['meetupDetail'][i]['image'];

location_event=data['payload']['meetupDetail'][i]['location'];
occupation=data['payload']['meetupDetail'][i]['occupation'];
languages=data['payload']['meetupDetail'][i]['languages'];
body_type=data['payload']['meetupDetail'][i]['body_type'];
height=data['payload']['meetupDetail'][i]['height'];
smoke=data['payload']['meetupDetail'][i]['smoke'];
drink=data['payload']['meetupDetail'][i]['drink'];

$("#newnonamers").append('<div class="col-md-4 events" id="newnonamers'+user_id+'" ><img onerror="$(this).remove()"class="profile_img" src="'+image+'"/><p>'+age+'</p></div>'); 
}*/


                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

function AddFavorite(user_id,USERID)
{


 $.ajax({
                            url: 'api/add-user-favorite',
                            type: 'POST',
                            data:{favorite_user_id:user_id,user_id:USERID,status:'1'},
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                          
                            },
                            success: function (data) {
                             
                      
                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });




}


function DeleteUserFavorite(user_id,USERID)
{

   $.ajax({
                            url: 'api/add-user-favorite',
                            data: {favorite_user_id:user_id,user_id:USERID,status:'0'},
                            type: 'POST',
                            dataType: 'json',
                        headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                         
                            },
                            complete: function (data) {
                                
                            },
                            success: function (data) {

                               if(data['code'])
                               {
                               
 alert(data['message']);

                               }else{
                                   alert("Not");
                               }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
}


                        </script>
@endsection
@extends('layouts.head')
