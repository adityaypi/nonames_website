@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">Pick as many as you like</div>
                <div class="card-body">

<form method="post" action="" id="profileupdate"  enctype="multipart/form-data">
<input type="date" name="dob" id="dob" class="form-control">	
<input type="hidden" name="age" id="age">
<select name="profession" id="profession"><option value="0">Profession</option>
</select>
<input type="submit" value="Continue" id="ContinueBtn">
</form>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

//all profession
 $.ajax({
                            url: 'api/profession',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                                
                            },
                            success: function (data) {

for ( i in data['payload']['allProfession'])
{
  $("#profession").append('<option value='+data['payload']['allProfession'][i]['profession_id']+'>'+data['payload']['allProfession'][i]['name']+'</option>');
}



                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

  
	$("#dob").change(function(){
var dob=$("#dob").val();

 var today = new Date();
    var birthDate = new Date($('#dob').val());
    var age = today.getFullYear() - birthDate.getFullYear();

    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
$("#age").val(age);
    });
$('form#profileupdate').submit(function (e) {

          e.preventDefault();
                        $.ajax({
                            url: 'api/update-profile',
                         data:  new FormData(this),
                            type: 'POST',
                              "processData": false,
                                 dataType: 'json',
  "contentType": false,
  "mimeType": "multipart/form-data",
                            headers: {
    "Authorization": AUTH_ADITYA,
        "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                                $("#ContinueBtn").val("Please wait..."); 
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                               if(data['code'])
                               {
                                   $("#ContinueBtn").val("Done"); 
                               	 window.location.href = 'add-user-language'; 
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                   

</script>


@endsection
@extends('layouts.head')
