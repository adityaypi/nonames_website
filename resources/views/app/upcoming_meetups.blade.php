@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-md-offset-3">
            <div class="card text-center">
                 <div class="card-header"><h2 class="pull-left"><a href="user-home">Back</a></h2></div>
                <div class="card-body">
                    <div class="text-center"><span id="totalupcoming" class="count-badge"></span></div>
<div class="row" id="upcomingmeetups">

</div>


                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	
$.ajax({
                            url: 'api/upcoming-meetups',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {
                                $("#totalupcoming").html(data['payload']['upcomingMeetups'].length);
for(i in data['payload']['upcomingMeetups'])
{
meetup_id=data['payload']['upcomingMeetups'][i]['meetup_id'];
user_id=data['payload']['upcomingMeetups'][i]['user_id'];
meetup_date_time=data['payload']['upcomingMeetups'][i]['meetup_date_time'];
request=data['payload']['upcomingMeetups'][i]['request'];
user_location=data['payload']['upcomingMeetups'][i]['location'];

$("#upcomingmeetups").append('<div onclick="meetup_by_id('+meetup_id+')" class="col-md-4 events"  ><form action="meetup-by-id" method="post" id="upcoming'+meetup_id+'"><input type="hidden" name="meetup_id" value="'+meetup_id+'">@csrf<p>'+user_location+" "+meetup_date_time+'</p></form></div>');

}



                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

function meetup_by_id(meetup_id)
{
   $("#upcoming"+meetup_id).submit();
}

</script>
@endsection
@extends('layouts.head')
