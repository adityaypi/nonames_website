@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">BODY TYPE</div>
                <div class="card-body">
<button type="btn btn-primary" value="1" class="body_type">Slim</button>
<button type="btn btn-primary" value="2" class="body_type">Athletic</button>
<button type="btn btn-primary" value="3" class="body_type">Average</button>
<button type="btn btn-primary" value="4" class="body_type">A few extra pounds</button>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	

$('.body_type').on('click',function (e) {
    var body_type=($(this).val());
    //var str = $("form#updatemeetwith").serialize();

          e.preventDefault();
                        $.ajax({
                            url: 'api/update-body-type',
                            data: {body_type:body_type},
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                            
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                             if(data['code'])
                               {
                                 
                              window.location.href = 'update-height';
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                    

</script>
@endsection
@extends('layouts.head')
