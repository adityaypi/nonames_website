@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">$</div>
                <div class="card-body">
<form method="post" action="" id="updatecontribute">
<div class="row" id="contribute">
<input type="radio" name="contribute" value="1"> Me
<input type="radio" name="contribute" value="2"> You
<input type="radio" name="contribute" value="3"> Split
</div>
<input type="submit" value="Continue" id="ContinueBtn">
</form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	

$('form#updatecontribute').submit(function (e) {
    var str = $("form#updatecontribute").serialize();

          e.preventDefault();
                        $.ajax({
                            url: 'api/update-contribute',
                            data: str,
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                               $("#ContinueBtn").val("Please wait...");  
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                             if(data['code'])
                               {
                                   $("#ContinueBtn").val("Done"); 
                               	   window.location.href = 'meet-with';
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                    

</script>
@endsection
@extends('layouts.head')
