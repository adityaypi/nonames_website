@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">HEIGHT</div>
                <div class="card-body">

<form method="post" action="" id="profileupdate"  enctype="multipart/form-data">
<input type="text" name="height_inch">
<input type="text" name="height_cm">
<input type="submit" value="Continue" id="ContinueBtn">
</form>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	
$('form#profileupdate').submit(function (e) {

          e.preventDefault();
                        $.ajax({
                            url: 'api/update-height',
                         data:  new FormData(this),
                            type: 'POST',
                              "processData": false,
                                 dataType: 'json',
  "contentType": false,
  "mimeType": "multipart/form-data",
                            headers: {
    "Authorization": AUTH_ADITYA,
        "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                                $("#ContinueBtn").val("Please wait..."); 
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                               if(data['code'])
                               {
                                   $("#ContinueBtn").val("Done"); 
                              window.location.href = 'update-drink'; 
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                   

</script>


@endsection
@extends('layouts.head')
