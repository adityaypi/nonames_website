@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">SMOKE</div>
                <div class="card-body">
<button type="btn btn-primary" value="1" class="smoke">No</button>
<button type="btn btn-primary" value="2" class="smoke">Socially</button>
<button type="btn btn-primary" value="3" class="smoke">Regularly</button>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	

$('.smoke').on('click',function (e) {
    var smoke=($(this).val());
    //var str = $("form#updatemeetwith").serialize();

          e.preventDefault();
                        $.ajax({
                            url: 'api/update-smoke',
                            data: {smoke:smoke},
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                            
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                             if(data['code'])
                               {
                                 
                              window.location.href = 'user-home';
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                    

</script>
@endsection
@extends('layouts.head')
