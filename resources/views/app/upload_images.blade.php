@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header"><h2>UPLOAD YOUR PHOTOS</h2><p><strong>No Names</strong> is about keeping it real. Please add atleast one photo of just yourself in the picture</p></div>
                <div class="card-body">

<form method="post" action="api/upload-images" id="uploadimages" enctype="multipart/form-data">
    <div class="row">
<div class="col-md-4">
<div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[0]" type="file" value="Choose a file">
    </div>
</div>
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" class="portimg" src="#"></div>
</div>
</div>

<div class="col-md-4">
 <div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[1]" type="file" value="Choose a file">
     </div>
</div> 
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" class="portimg" src="#"></div>
</div>
</div>

<div class="col-md-4">
 <div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[2]" type="file" value="Choose a file">
     </div>
</div> 
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" class="portimg" src="#"></div>
</div>
</div>

<div class="col-md-4">
 <div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[3]" type="file" value="Choose a file">
     </div>
</div> 
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" class="portimg" src="#"></div>
</div>
</div>

<div class="col-md-4">
 <div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[4]" type="file" value="Choose a file">
     </div>
</div> 
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" class="portimg" src="#"></div>
</div>
</div>

<div class="col-md-4">
 <div class="form-group hirehide is-empty is-fileinput width100">
    <div class="socialmediaside2">
        <input class="fileUpload" accept="image/jpeg, image/jpg" name="image[5]" type="file" value="Choose a file">
     </div>
</div> 
<div class="upload-demo">
    <div class="upload-demo-wrap"><img alt="your image" class="portimg" src="#"></div>
</div>
</div>
<div class="col-md-12 text-center">
<input type="submit" value="Continue" id="ContinueBtn">
</div>
</div>
</form>


                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	
	    function readURL() {
        var $input = $(this);
        var $newinput =  $(this).parent().parent().parent().find('.portimg ');
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                reset($newinput.next('.delbtn'), true);
                $newinput.attr('src', e.target.result).show();
                $newinput.after('<input type="button" class="delbtn removebtn" value="remove">');
            }
            reader.readAsDataURL(this.files[0]);
        }
    }
    $(".fileUpload").change(readURL);
    $("form").on('click', '.delbtn', function (e) {
        reset($(this));
    });

    function reset(elm, prserveFileName) {
        if (elm && elm.length > 0) {
            var $input = elm;
            $input.prev('.portimg').attr('src', '').hide();
            if (!prserveFileName) {
                $($input).parent().parent().parent().find('input.fileUpload ').val("");
                //input.fileUpload and input#uploadre both need to empty values for particular div
            }
            elm.remove();
        }
    }


$('form#uploadimages').submit(function (e) {

          e.preventDefault();
                        $.ajax({
                            url: 'api/upload-images',
                         data:  new FormData(this),
                            type: 'POST',
                              "processData": false,
                                 dataType: 'json',
  "contentType": false,
  "mimeType": "multipart/form-data",
                            headers: {
    "Authorization": AUTH_ADITYA,
        "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                                $("#ContinueBtn").val("Please wait..."); 
                            },
                            complete: function (data) {
                                	
                            },
                            success: function (data) {
                               if(data['code'])
                               {
                                   $("#ContinueBtn").val("Done"); 
                               	  window.location.href = 'profile-update'; 
                               }else
                               {

                               }    
                             
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    });
                   

</script>


<style type="text/css">
	img.portimg {
            display: none;
            max-width: 200px;
            max-height: 200px;
}
</style>
@endsection
@extends('layouts.head')