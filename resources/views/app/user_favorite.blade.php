@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                 <div class="card-header"><h2 class="pull-left"><a href="user-home">Back</a></h2><h2 class="text-center">Favorite</h2></div>
                <div class="card-body">

<div class="row" id="userfavorite">

</div>


                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	
$.ajax({
                            url: 'api/user-favorite',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {
for(i in data['payload']['allFavoriteUser'])
{
user_id=data['payload']['allFavoriteUser'][i]['user_id'];
age=data['payload']['allFavoriteUser'][i]['age'];
favorite_user_id=data['payload']['allFavoriteUser'][i]['favorite_user_id'];
image=data['payload']['allFavoriteUser'][i]['image'];
user_location=data['payload']['allFavoriteUser'][i]['location'];
user_id=data['payload']['allFavoriteUser'][i]['user_id'];

$("#userfavorite").append('<div onclick="nonamer_detail_by_id('+favorite_user_id+')" class="col-md-4 events"  ><form action="nonamer-detail-by-id" method="post" id="favorite'+favorite_user_id+'"><input type="hidden" name="route" value="favorite"><input type="hidden" name="user_id" value="'+favorite_user_id+'">@csrf<img class="profile_img" src="'+image+'"/><p>'+age+'</p></form></div>');
}

},
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });


function nonamer_detail_by_id(user_id)
{
   $("#favorite"+user_id).submit();
}

</script>
@endsection
@extends('layouts.head')
