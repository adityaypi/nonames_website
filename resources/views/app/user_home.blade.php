@extends('layouts.footer')
@extends('layouts.nav')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card text-center">
                <div class="card-header">USER HOME</div>
                <div class="card-body">

<div class="row">
	<div class="col-md-12">
		<h2>INVITES</h2>
        <div class="row" id="invites"></div>
	</div>
	<div class="col-md-12" id="">
		<h2>NEW NO NAMERS</h2>
        <div class="row" id="newnonamers"></div>
	</div>
	<div class="col-md-12" id="">
		<h2>BY MEETUP</h2>
        <div class="row" id="bymeetup"></div>
	</div>
	<div class="col-md-12" id="">
		<h2>MY MEETUPS</h2>
        <div class="row" id="mymeetup"></div>
        <a href="create-meetup">Create Meetup</a>
	</div>

</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
// ALL MEETUP	


                        $.ajax({
                            url: 'api/dashboard',
                            type: 'POST',
                            dataType: 'json',
                            headers: {
    "Authorization": AUTH_ADITYA,
    "Accept": "application/json",
    "cache-control": "no-cache",
                            },
                            beforeSend: function () {
                        
                            },
                            complete: function (data) {
                               	
                            },
                            success: function (data) {
for(i in data['payload']['availableMeetups'])
{
meetup_id=data['payload']['availableMeetups'][i]['meetup_id'];
event=data['payload']['availableMeetups'][i]['event'];
image=data['payload']['availableMeetups'][i]['image'];
meetup_creator_id=data['payload']['availableMeetups'][i]['meetup_creator_id'];
$("#invites").append('<div onclick="meetup_detail_by_id('+meetup_id+')" class="col-md-4 events" id="invites'+meetup_id+'" ><form action="meetup-detail-by-id" method="post" id="invite'+meetup_id+'"><input type="hidden" name="meetup_id" value="'+meetup_id+'">@csrf<img class="profile_img" onerror="$(this).remove()" src="'+image[0]+'"/><p>'+event+" "+meetup_creator_id+'</p></form></div>');
}

for(i in data['payload']['newNoNamers'])
{
user_id=data['payload']['newNoNamers'][i]['user_id'];
age=data['payload']['newNoNamers'][i]['age'];
image=data['payload']['newNoNamers'][i]['image'];
$("#newnonamers").append('<div onclick="nonamer_detail_by_id('+user_id+')" class="col-md-4 events" id="new'+user_id+'" ><form action="nonamer-detail-by-id" method="post" id="favorite'+user_id+'"><input type="hidden" name="route" value="home"><input type="hidden" name="user_id" value="'+user_id+'">@csrf</form></div>');
for(img in image){
$('#new'+user_id+'').append('<img class="profile_img" onerror="$(this).remove()" src="'+image[img]+'"/>');
break;
}
$('#new'+user_id+'').append('<p>'+age+' '+user_id+'</p>');
}

for(i in data['payload']['byMeetup'])
{
event_id=data['payload']['byMeetup'][i]['event_id'];
event_name=data['payload']['byMeetup'][i]['event_name'];
icon_white=data['payload']['byMeetup'][i]['icon_white'];
icon_dark=data['payload']['byMeetup'][i]['icon_dark'];
status=data['payload']['byMeetup'][i]['status'];
$("#bymeetup").append('<div onclick="all_meetup_by_event('+event_id+')" class="col-md-4 events" id="byMeetup'+event_id+'" ><form action="all-meetup-by-event" method="post" id="allmeetup'+event_id+'"><input type="hidden" name="event_id" value="'+event_id+'">@csrf<img src="'+icon_dark+'"/><p>'+event_name+'</p></form></div>');
}

for(i in data['payload']['myMeetups'])
{
meetup_id=data['payload']['myMeetups'][i]['meetup_id'];
user_id=data['payload']['myMeetups'][i]['user_id'];
event_id=data['payload']['myMeetups'][i]['event_id'];
event_location=data['payload']['myMeetups'][i]['location'];
meetup_date_time=data['payload']['myMeetups'][i]['meetup_date_time'];
meetup_status=data['payload']['myMeetups'][i]['meetup_status'];

$("#mymeetup").append('<div onclick="meetup_by_id('+meetup_id+')" class="col-md-4 events"  ><form action="meetup-by-id" method="post" id="mymeetup'+meetup_id+'"><input type="hidden" name="meetup_id" value="'+meetup_id+'">@csrf<p>'+event_location+meetup_date_time+" "+meetup_status+'</p></form></div>');
}



                     },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

function meetup_detail_by_id(meetup_id)
{
   $("#invite"+meetup_id).submit();
}
function meetup_by_id(meetup_id)
{
   $("#mymeetup"+meetup_id).submit();
}
function nonamer_detail_by_id(user_id)
{
   $("#favorite"+user_id).submit();
}
function all_meetup_by_event(event_id)
{
   $("#allmeetup"+event_id).submit();
}
</script>
@endsection
@extends('layouts.head')
