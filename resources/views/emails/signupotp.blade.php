<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
<title>Respmail is a response HTML email designed to work on all major email platforms and smartphones</title>
<style type="text/css">
    /* RESET STYLES */
    body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family: Arial,Helvetica Neue,Helvetica,sans-serif;}
    table{border-collapse:collapse;}
    table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#000;font-weight:normal;}
    img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
    a {text-decoration:none !important;border-bottom: 1px solid;}
    .btn{border:1px solid #61244c; height: 20px; line-height: 20px; font-size: 12px; color:#7E2F5F; display: inline-block; margin: 5px; padding: 0 10px; cursor: pointer;}
    .text-field{width: 100%; height: 60px; border: 1px solid #f6f6f6; margin: 0 0 10px; padding: 12px; box-sizing: border-box;}
    h1{text-transform: uppercase;}
</style>
</head>
<body bgcolor="#f6f6f6" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    <center style="background-color:#f6f6f6;">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
            <tr>
                <td align="center" valign="top" id="bodyCell">

                   

                    <table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">


                        <tr>
                            <td align="center" valign="top">

                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF; border-top:4px solid #ca3533" bgcolor="#fff">
                                    <tr>
                                        <td align="left" valign="middle" style="padding:20px 30px 5px;">
                                            <img src="{{ url('/assets/images/logo-lg.png')}}" width="118" height="46">
                                        </td>
                                        <td align="right" valign="middle" style="padding:20px 30px 5px;">
                                            <p style="color:#000;">&nbsp;</p>
                                        </td>
                                    </tr>
                                </table>
                                <!-- // CENTERING TABLE -->
                            </td>
                        </tr>

                        <tr mc:hideable>

                            <td align="center" valign="top" style="padding: 0 30px 10px;">
                                <!-- CENTERING TABLE // -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top:2px solid #F8F8F8">
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- FLEXIBLE CONTAINER // -->

                                            <table border="0" cellspacing="0" width="500" class="flexibleContainer">
                                                <tr>
                                                    <td valign="top" width="500" class="flexibleContainerCell">
                                                        <!-- CONTENT TABLE // -->
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left" width="100%" valign="middle" class="flexibleContainerBox" style="padding:14px 0px; border-bottom:1px solid #F8F8F8;font-size:12px">
                                                                    <p>You are receiving this email for OTP.</p>
                                                                    <p>OTP verification code  {{ $data["verification code"] }}</p>
                                                                    <p>If you did not reques2t a OTP, no further action is required.</p>
                                                                </td>
                                                            </tr>
                                                            
                                                        </table>
                                                        <!-- // CONTENT TABLE -->

                                                    </td>
                                                </tr>
                                            </table>



                                            <!-- // FLEXIBLE CONTAINER -->
                                        </td>
                                    </tr>
                                </table>
                                <!-- // CENTERING TABLE -->
                            </td>
                        </tr>




                        <table bgcolor="#f6f6f6" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">


                            <tr>
                                <td align="center" valign="top">
                                    <!-- CENTERING TABLE // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" valign="top">
                                                <!-- FLEXIBLE CONTAINER // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                    <tr>
                                                        <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" bgcolor="#f6f6f6">

                                                                        <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                            <div>Copyright &#169; 2018  All&nbsp;rights&nbsp;reserved.</div>
                                                                            <div>If you do not want to recieve emails from us, you can <a href="#" target="_blank" style="text-decoration:none;color:#828282;"><span style="color:#828282;">unsubscribe</span></a>.</div>
                                                                        </div>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // FLEXIBLE CONTAINER -->
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // CENTERING TABLE -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END -->
                </td>
            </tr>
        </table>
    </center>
</body>
</html>
