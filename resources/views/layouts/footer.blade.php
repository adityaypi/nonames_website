

<div class="" id="footer">
<div class="container">
 <div class="row"> 
<div class="col-md-4 py-4">
<h4 class="py-4">Contact</h4>
<ul class="nospace btmspace-30 linklist contact">
        <li>
          <address><i class="fa fa-map-marker"></i>
          Street Name &amp; Number, Town, Postcode / Zip
          </address>
        </li>
        <li><i class="fa fa-phone"></i> +00 (123) 456 7890</li>
        <li><i class="fa fa-fax"></i> +00 (123) 456 7890</li>
        <li><i class="fa fa-envelope-o"></i> info@nonames.com</li>
      </ul>
</div>
<div class="col-md-4 py-4">
  <h4 class="py-4">APP Store</h4>
<div class="col-md-7 col-sm-3 col-xs-3 py-4 object-center">
<img src="assets/images/appStore.png"  class="img-fluid app-icon">
</div>
<div class="col-md-7 col-sm-3 col-xs-3 object-center">
<img src="assets/images/googleStore.png"  class="img-fluid app-icon">
</div>
</div>
<div class="col-md-4 py-4">
<h4 class="py-4">Get in Touch!</h4>
<form method="post" action="" class="footer-form">
  <input type="text" name="name" class="form-control" required placeholder="Name" autocomplete="off">
  <input type="email" name="email" class="form-control" required placeholder="Email" autocomplete="off">
  <input type="submit" class="btn btn-primary">
</form>
</div>
</div>
</div>
</div>

<footer class="bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright © NO NAMES 2019</p>
      </div>    
</footer> 
    <a id="backtotop" href="#top" class=""><i class="fa fa-chevron-up"></i></a>
    
</body>
</html>