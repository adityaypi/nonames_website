<div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel fixed-top">
            <div class="container">                
           <!-- <ul class="navbar-nav">
        <li class=""><i class="fa fa-phone"></i> +00 (123) 456 7890</li> 
        <li class=""><i class="fa fa-envelope-o"></i> info@domain.com</li>
      </ul> -->      
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/"><i class="fa fa-home fa-2x"></i></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">{{ __('About Us') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">{{ __('Privacy Policy') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">{{ __('Contact Us') }}</a>
                            </li>                  
 <li class="dropdown">
    <span class="dropdown-toggle nav-link"  data-toggle="dropdown">Links
    <span class="caret"></span></span>
    <ul class="dropdown-menu">
    <li class="nav-item"><a class="nav-link"  href="user-favorite">Favorite</a></li>
    <li class="nav-item"><a class="nav-link"  href="upcoming-meetups">Upcoming Meetups</a></li>
    <li class="nav-item"><a class="nav-link"  href="my-profile">My Profile</a></li>
    </ul>
  </li>
                    </ul>
                </div>
            </div>
        </nav>

    </div>

   <div class="container"> 
    <header id="header" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div id="logo" class="fl_left">
    <a href="/"><img src="assets/images/logo.png"  class="img-fluid"></a>
    </div>
    <div id="quickinfo" class="fl_right">
      <ul class="inline">
        <li><strong> <span id="timer" class="text-right"></span></strong><br>
    </ul>
    </div>
    <!-- ################################################################################################ -->
  </header>
</div>
<main class="py-4">
@yield('content')
</main>