<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'Api\V1\ApiController@login');
Route::post('register', 'Api\V1\ApiController@register');
Route::post('generate-otp', 'Api\V1\ApiController@generateOtp');
Route::post('verify-otp', 'Api\V1\ApiController@verifyOtp');
Route::post('country', 'Api\V1\ApiController@country');
Route::group(['middleware' => 'auth:api'], function(){
    //Master api
    Route::post('events', 'Api\V1\ApiController@events');
    Route::post('language', 'Api\V1\ApiController@language');
    Route::post('profession', 'Api\V1\ApiController@profession');
    //Profile Detail
    Route::post('my-profile', 'Api\V1\ApiController@myProfile');
    //Update User event
    Route::post('add-user-event', 'Api\V1\ApiController@addUserEvent');
    Route::post('update-user-event', 'Api\V1\ApiController@updateUserEvent');
    Route::post('delete-user-event', 'Api\V1\ApiController@deleteUserEvent');
    Route::post('user-event', 'Api\V1\ApiController@userEvent');
    //Update User meet with
    Route::post('update-meet-with', 'Api\V1\ApiController@updateMeetwith');
    //Update User contribute
    Route::post('update-contribute', 'Api\V1\ApiController@updateContribute');
    //Update User Image
    Route::post('upload-images', 'Api\V1\ApiController@uploadImages');
    Route::post('delete-image', 'Api\V1\ApiController@deleteImage');
    //Update User Profile
    Route::post('update-profile', 'Api\V1\ApiController@updateProfile');
    //Update User Gender
    Route::post('update-gender', 'Api\V1\ApiController@updateGender');
    //Update User personal detail
    Route::post('update-personal', 'Api\V1\ApiController@updatePersonal');
    //Update User preferences detail
    Route::post('update-preferences', 'Api\V1\ApiController@updatePreferences');
    //Update User language
    Route::post('add-user-language', 'Api\V1\ApiController@addUserLanguage');
    Route::post('update-user-language', 'Api\V1\ApiController@updateUserLanguage');
    Route::post('delete-user-language', 'Api\V1\ApiController@deleteUserLanguage');
    Route::post('user-language', 'Api\V1\ApiController@userLanguage');
    //Update User body type
    Route::post('update-body-type', 'Api\V1\ApiController@updateBodyType');
    //Update User height
    Route::post('update-height', 'Api\V1\ApiController@updateHeight');
    //Update User drink
    Route::post('update-drink', 'Api\V1\ApiController@updateDrink');
    //Update User smoke
    Route::post('update-smoke', 'Api\V1\ApiController@updateSmoke');
    //Update User phone number
    Route::post('update-phone', 'Api\V1\ApiController@updatePhone');
    //Update User favorite
    Route::post('add-user-favorite', 'Api\V1\ApiController@addUserFavorite');
    Route::post('update-user-favorite', 'Api\V1\ApiController@updateUserFavorite');
    Route::post('delete-user-favorite', 'Api\V1\ApiController@deleteUserFavorite');
    Route::post('user-favorite', 'Api\V1\ApiController@userFavorite');
    //Dashboard 
    Route::post('dashboard', 'Api\V1\ApiController@dashboard');
    //Available meetups 
    Route::post('available-meetups', 'Api\V1\ApiController@availableMeetups');
    //All meetups
    Route::post('all-meetup', 'Api\V1\ApiController@allMeetup');
    //All new namer
    Route::post('all-new-namer', 'Api\V1\ApiController@allNewNamer');
    //All meetups by event
    Route::post('all-meetup-by-event', 'Api\V1\ApiController@allMeetupByEvent');
    //Meetup detail by id
    Route::post('meetup-detail-by-id', 'Api\V1\ApiController@meetupDetailById');
    //Meetup request sent
    Route::post('meetup-request-sent', 'Api\V1\ApiController@meetupRequestSent');
    //Meetup request cancelled
    Route::post('meetup-request-cancelled', 'Api\V1\ApiController@meetupRequestCancelled');
    //Meetup request accept
    Route::post('meetup-request-accept', 'Api\V1\ApiController@meetupRequestAccept');
    //No namer detail by id
    Route::post('nonamer-detail-by-id', 'Api\V1\ApiController@nonamerDetailById');
    //My meetups list by id
    Route::post('my-meetups-by-id', 'Api\V1\ApiController@myMeetupsById');
    //Request sent list by id
    Route::post('request-sent-by-id', 'Api\V1\ApiController@requestSentById');
    //Meetup create, edit & cancel
    Route::post('create-meetup', 'Api\V1\ApiController@createMeetup');
    Route::post('edit-meetup', 'Api\V1\ApiController@editMeetup');
    Route::post('cancel-meetup', 'Api\V1\ApiController@cancelMeetup');
    //Meetup list
    Route::post('upcoming-meetups', 'Api\V1\ApiController@upcomingMeetups');
    Route::post('past-meetups', 'Api\V1\ApiController@pastMeetups');
    Route::post('meetup-by-id', 'Api\V1\ApiController@meetupById');
});