<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


//Route::get('/', function () {
  //  return view('welcome');
//});

Route::get('/', function () {
   return view('welcome');
});

Route::get('/generate-otp', function () {
    return view('app.generate_otp');
});

Route::get('/update-gender', function () {
    return view('app.update_gender');
});

Route::get('/events-list', function () {
    return view('app.events_list');
});

Route::get('/meet-with', function () {
    return view('app.meet_with');
});

Route::get('/update-contribute', function () {
    return view('app.update_contribute');
});

Route::get('/upload-image', function () {
    return view('app.upload_images');
});

Route::get('/profile-update', function () {
    return view('app.profile_update');
});

Route::get('/add-user-language', function () {
    return view('app.add_user_language');
});

Route::get('/update-body-type', function () {
    return view('app.update_body_type');
});

Route::get('/update-height', function () {
    return view('app.update_height');
});

Route::get('/update-drink', function () {
    return view('app.update_drink');
});

Route::get('/update-smoke', function () {
    return view('app.update_smoke');
});

Route::get('/user-home', function () {
    return view('app.user_home');
});

Route::get('/create-meetup', function () {
    return view('app.create_meetup');
});

Route::post('/meetup-detail-by-id', function () {
    return view('app.meetup_detail_by_id');
});

Route::post('/nonamer-detail-by-id', function () {
    return view('app.nonamer_detail_by_id');
});

Route::post('/all-meetup-by-event', function () {
    return view('app.all_meetup_by_event');
});

Route::get('/user-favorite', function () {
    return view('app.user_favorite');
});

Route::get('/upcoming-meetups', function () {
    return view('app.upcoming_meetups');
});

Route::post('/meetup-by-id', function () {
    return view('app.meetup_by_id');
});

Route::get('/my-profile', function () {
    return view('app.my_profile');
});

Auth::routes();

Route::get('/about', 'WebsiteController@about')->name('about');
Route::get('/contact', 'WebsiteController@contact')->name('contact');
//Route::get('/', 'WebsiteController@index')->name('home1');
Route::get('/home1', 'WebsiteController@index')->name('home1');
Route::get('/home', 'HomeController@index')->name('home');

//admin section
Route::get('/admin/index', function () {
	return view('admin.index');
});
Route::post('/admin/index', 'Admin\AdminController@signIn');
Route::group(['middleware' => 'checkadmin', 'roles' => ['Admin']], function () {

	Route::get('/admin/dashboard', 'Admin\AdminController@dashboard');

	//edit profile
	Route::get('/admin/edit-profile', 'Admin\AdminController@editProfile');
	Route::post('/admin/edit-profile', 'Admin\AdminController@saveEditProfile');
	//change password
	Route::get('/admin/change-password', 'Admin\AdminController@changePassword');
	Route::post('/admin/change-password', 'Admin\AdminController@saveChangePassword');

	Route::get('/admin/logout', 'Admin\AdminController@logout');

	//Register
	Route::get('/admin/register/list', 'Admin\AdminController@listRegester');
	Route::get('/admin/ajaxListRegester', 'Admin\AdminController@ajaxListRegester');
	Route::post('/admin/changeRegesterStatus', 'Admin\AdminController@changeRegesterStatus');
	Route::post('/admin/deleteRegester', 'Admin\AdminController@deleteRegester');
	Route::get('/admin/register/view/{id}', 'Admin\AdminController@viewRegester');

	//Meetup
	Route::get('/admin/meetup/list', 'Admin\MeetupController@listMeetup');
	Route::get('/admin/ajaxListMeetup', 'Admin\MeetupController@ajaxListMeetup');
	Route::post('/admin/changeMeetupStatus', 'Admin\MeetupController@changeMeetupStatus');
	Route::post('/admin/deleteMeetup', 'Admin\MeetupController@deleteMeetup');
	Route::get('/admin/meetup/view/{id}', 'Admin\MeetupController@viewMeetup');

	//CMS
	Route::get('/admin/cms/list', 'Admin\CmsController@listCms');
	Route::get('/admin/ajaxListCms', 'Admin\CmsController@ajaxListCms');
	Route::post('/admin/changeCmsStatus', 'Admin\CmsController@changeCmsStatus');
	Route::post('/admin/deleteCms', 'Admin\CmsController@deleteCms');
	Route::get('/admin/cms/view/{id}', 'Admin\CmsController@viewCms');

	//Events
	Route::get('/admin/events/list', 'Admin\EventsController@listEvents');
	Route::get('/admin/ajaxListEvents', 'Admin\EventsController@ajaxListEvents');
	Route::post('/admin/changeEventsStatus', 'Admin\EventsController@changeEventsStatus');
	Route::post('/admin/deleteEvents', 'Admin\EventsController@deleteEvents');
	Route::get('/admin/events/view/{id}', 'Admin\EventsController@viewEvents');

	//Language
	Route::get('/admin/language/list', 'Admin\LanguageController@listLanguage');
	Route::get('/admin/ajaxListLanguage', 'Admin\LanguageController@ajaxListLanguage');
	Route::post('/admin/changeLanguageStatus', 'Admin\LanguageController@changeLanguageStatus');
	Route::post('/admin/deleteLanguage', 'Admin\LanguageController@deleteLanguage');
	Route::get('/admin/language/view/{id}', 'Admin\LanguageController@viewLanguage');

	//Profession
	Route::get('/admin/profession/list', 'Admin\ProfessionController@listProfession');
	Route::get('/admin/ajaxListProfession', 'Admin\ProfessionController@ajaxListProfession');
	Route::post('/admin/changeProfessionStatus', 'Admin\ProfessionController@changeProfessionStatus');
	Route::post('/admin/deleteProfession', 'Admin\ProfessionController@deleteProfession');
	Route::get('/admin/profession/view/{id}', 'Admin\ProfessionController@viewProfession');

});
